package pl.akademiakodu.docker;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class CityController {


    private CityRepository cityRepository;

    public CityController(CityRepository cityRepository){
        this.cityRepository = cityRepository;
    }

    @GetMapping("/")
    public Iterable<City> list(){
        return cityRepository.findAll();
    }


}
